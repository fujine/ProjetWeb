<?php

namespace LoveLetterBundle\Entity;

define("NEWGAME", 0);
define("NEWROUND", 1);
define("DRAW", 2);
define("PLAYGUARDTRUE", 3);
define("PLAYGUARDFALSE", 4);
define("PLAY", 5);
define("ENDROUND", 6);
define("ELIM", 7);
define("NOEFFECT", 8);
define("DISCARD", 9);
define("ENDDECK",10);
define("DRAWEND", 11);
define("ENDGAME", 12);
define("LEAVE", 13);

use Doctrine\ORM\Mapping as ORM;

/**
 * History
 *
 * @ORM\Table(name="history")
 * @ORM\Entity(repositoryClass="LoveLetterBundle\Repository\HistoryRepository")
 */
class History
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
    * @ORM\ManyToOne(targetEntity="LoveLetterBundle\Entity\Game", inversedBy="history")
    * @ORM\JoinColumn(nullable=false)
    */
    private $game;
    
    /**
     * 
     * @var integer
     * 
     * @ORM\Column(name="indexHisto", type="integer")
     * @ORM\JoinColumn(nullable=false)
     */
    private $indexHisto;
    
    /**
     * @var int
     *
     * @ORM\Column(name="actionName", type="integer")
     * @ORM\JoinColumn(nullable=false)
     */
    private $actionName;

    /**
     * @var int
     * 
     * @ORM\Column(name="cardId", type="integer", nullable=true)
     */
    private $cardId;

    /**
     * @var int
     *
     * @ORM\Column(name="player_id", type="integer", nullable=true)
     */
    private $playerId;

    /**
     * @var int
     *
     * @ORM\Column(name="playerTarget_id", type="integer", nullable=true)
     */
    private $playerTargetId;
    
    /**
     * @var int
     * 
     * @ORM\Column(name="type", type="integer", nullable=true)
     */
    private $type;

    /**
     * @var array
     * 
     * @ORM\Column(name="win", type="array", nullable=true)
     */
    private $win;

    /**
     * Constructo
     * 
     * @param int $indexHisto
     * @param \LoveLetterBundle\Entity\Game $gameId
     * @param string $actionName
     * @param int $playerId
     * @param int $playerTargetId
     * @param array $cardPlayed
     */
    public function __construct($indexHisto, $gameId, $actionName, $playerId, $playerTargetId, $cardId, $type,$win){
        $this->indexHisto = $indexHisto;
        $this->game = $gameId;
        $this->actionName = $actionName;
        $this->playerId = $playerId;
        $this->playerTargetId = $playerTargetId;
        $this->cardId = $cardId;
        $this->type = $type;
        $this->win = $win;
    }
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set actionName
     *
     * @param string $actionName
     *
     * @return History
     */
    public function setActionName($actionName)
    {
        $this->actionName = $actionName;

        return $this;
    }

    /**
     * Get actionName
     *
     * @return string
     */
    public function getActionName()
    {
        return $this->actionName;
    }

    /**
     * Set gameId
     *
     * @param integer $gameId
     *
     * @return History
     */
    public function setGameId($gameId)
    {
        $this->gameId = $gameId;

        return $this;
    }

    /**
     * Get gameId
     *
     * @return int
     */
    public function getGameId()
    {
        return $this->gameId;
    }

    /**
     * Set cardPlayed
     *
     * @param string $cardPlayed
     *
     * @return History
     */
    public function setCardPlayed($cardPlayed)
    {
        $this->cardId = $cardPlayed;

        return $this;
    }

    /**
     * Get cardPlayed
     *
     * @return string
     */
    public function getCardPlayed()
    {
        return $this->cardId;
    }

    /**
     * Set playerId
     *
     * @param integer $playerId
     *
     * @return History
     */
    public function setPlayerId($playerId)
    {
        $this->playerId = $playerId;

        return $this;
    }

    /**
     * Get playerId
     *
     * @return int
     */
    public function getPlayerId()
    {
        return $this->playerId;
    }

    /**
     * Set playerTargetId
     *
     * @param integer $playerTargetId
     *
     * @return History
     */
    public function setPlayerTargetId($playerTargetId)
    {
        $this->playerTargetId = $playerTargetId;

        return $this;
    }

    /**
     * Get playerTargetId
     *
     * @return int
     */
    public function getPlayerTargetId()
    {
        return $this->playerTargetId;
    }

    /**
     * Set indexHisto
     *
     * @param integer $indexHisto
     *
     * @return History
     */
    public function setIndexHisto($indexHisto)
    {
        $this->indexHisto = $indexHisto;

        return $this;
    }

    /**
     * Get indexHisto
     *
     * @return integer
     */
    public function getIndexHisto()
    {
        return $this->indexHisto;
    }

    /**
     * Set game
     *
     * @param \LoveLetterBundle\Entity\Game $game
     *
     * @return History
     */
    public function setGame(\LoveLetterBundle\Entity\Game $game)
    {
        $this->game = $game;

        return $this;
    }

    /**
     * Get game
     *
     * @return \LoveLetterBundle\Entity\Game
     */
    public function getGame()
    {
        return $this->game;
    }
    
    /*
     * 
     * to String
     * 
     * Génère un un historique en fonction des informations enregistrées dans la base de données
     */
    
    public function toString(){
        $playerName = "";
        $playerTargetName = "";

        //S'il y a un joueur alors on enregistre son nom
        if($this->playerId != null){
            $playerName = $this->game->getPlayerById($this->playerId)->getUsername();
        }

        //S'il y a une cible alors on enregistre son nom
        if($this->playerTargetId != null){
            $playerTargetName = $this->game->getPlayerById($this->playerTargetId)->getUsername();
        }

        //Début du string généré
        $str = "<span class=\"boldFont\">".$this->indexHisto."</span> | ";

        switch ($this->actionName){

            case NEWGAME: // Nouvelle partie
                $str = $str."<span class=\"boldFont\">Création de la partie.</span>";
                break;

            case NEWROUND : // Nouvelle manche
                $str = $str."<span class=\"boldFont\">Nouvelle manche.</span>";
                break;

            case DRAW: // Pioche
                $str = $str."<span class=\"redFont\">".$playerName."</span> a pioché une carte.";
                break;

            case PLAY: // Jouer une carte qui n'est pas le garde

                // Récuperation de la carte par son ID
                $card = $this->game->getCardById($this->cardId);

                // Création du la partie commune du string
                $str = $str."<span class=\"redFont\">".$playerName."</span> a joué ".$card->getName();

                if($card->getType()== 2) {
                // Carte jouée = Priest

                    $str = $str." et a regardé la main de <span class=\"redFont\">".$playerTargetName."</span>.";

                } else if($card->getType()== 3) {
                // Carte jouée = Baron

                    // Récuperation des cartes comparées entre les 2 joueurs
                    $type = $this->win[0];
                    $typeTarget = $this->win[1];
                    if($type < $typeTarget){
                        $str = $str." et a perdu le duel contre <span class=\"redFont\">".$playerTargetName."</span>.";
                    } else if($type > $typeTarget){
                        $str = $str." et a gagné le duel contre <span class=\"redFont\">".$playerTargetName."</span>.";
                    } else {
                        $str = $str." et il n'y a pas eu de gagnant au duel avec <span class=\"redFont\">".$playerTargetName."</span>.";
                    }
                } else if($card->getType() == 4) {
                    $str = $str." et est protégé pendant 1 tour.";
                } else if($card->getType() == 5) {
                    if($this->playerId == $this->playerTargetId) {
                        $str = $str." et l'a utilisé sur lui même.";
                    } else {
                        $str = $str." et l'a utilisé sur <span class=\"redFont\">".$playerTargetName."</span>.";
                    }
                } else if($card->getType() == 6) {
                    $str = $str." et a échangé sa main avec <span class=\"redFont\">".$playerTargetName."</span>.";
                } else if($card->getType() == 7) {
                    $str = $str.".";
                }
                break;
            case PLAYGUARDFALSE:
                $card = $this->game->getCardById($this->cardId);
                $str = $str."<span class=\"redFont\">".$playerName."</span> a joué ".$card->getName();
                $str = $str." et a demandé la carte ".$this->getName($this->type)." à <span class=\"redFont\">".$playerTargetName."</span> qui ne la possède pas.";
                break;
            case PLAYGUARDTRUE:
                $card = $this->game->getCardById($this->cardId);
                $str = $str."<span class=\"redFont\">".$playerName."</span> a joué ".$card->getName();
                $str = $str." et a demandé la carte ".$this->getName($this->type)." à <span class=\"redFont\">".$playerTargetName."</span> qui la possède.";
                break;
            case ELIM :
                $str = $str."<span class=\"redFont\">".$playerName."</span> est éliminé.";
                break;
            case ENDROUND :
                $str = $str."Le gagnant est <span class=\"redFont\">".$playerName."</span>.";
                break;
            case NOEFFECT :
                $card = $this->game->getCardById($this->cardId);
                $str = $str."<span class=\"redFont\">".$playerName."</span> a joué ".$card->getName()." sans effet.";
                break;
            case DISCARD :
                $card = $this->game->getCardById($this->cardId);
                $str = $str."<span class=\"redFont\">".$playerName."</span> a défaussé ".$card->getName().".";
                break;
            case ENDDECK :
                if(count($this->win) == 1){
                    $playerName = $this->game->getPlayerById($this->win[0])->getUsername();
                    $str = $str."<span class=\"redFont\">".$playerName."</span> a gagné la manche avec la carte ".$this->getName($this->type).".";
                }else {
                    $str = $str."Les joueurs ";
                    for($i = 0; $i < count($this->win); $i++){
                        $playerName = $this->game->getPlayerById($this->win[$i])->getUsername();
                        $str = $str."<span class=\"redFont\">".$playerName."</span> ";
                    }
                    $str = $str." ont gagnés la manche avec la carte ".$this->getName($this->type).".";
                }
                break;
            case DRAWEND : 
                $str = $str."<span class=\"redFont\">".$playerName."</span> a pioché la carte ".$this->getName($this->type).".";
                break;
            case LEAVE :
                $str = $str."<span class=\"redFont\">".$playerName."</span> a quitté la partie.";
                break;
            case ENDGAME :
                if(count($this->win) == 1){
                    $playerName = $this->game->getPlayerById($this->win[0])->getUsername();
                    $str = $str."<span class=\"redFont\">".$playerName."</span> a gagné la partie.";
                }else {
                    $str = $str."Les joueurs ";
                    for($i = 0; $i < count($this->win); $i++){
                        $playerName = $this->game->getPlayerById($this->win[$i])->getUsername();
                        $str = $str."<span class=\"redFont\">".$playerName."</span> ";
                    }
                    $str = $str." ont gagnés la partie.";
                }
                break;
            default:
                $str = $str."Erreur de l'historique!";
                break;
        }
        return $str;
    }
    
    public function getName($type){
        switch($type){
            case 1:
                return "Guard";
            case 2:
                return "Priest";
            case 3:
                return "Baron";
            case 4:
                return "Handmain";
            case 5:
                return "Prince";
            case 6:
                return "King";
            case 7:
                return "Countess";
            case 8:
                return "Princess";
            default:
                return "Not a card";
        }
    }

}
