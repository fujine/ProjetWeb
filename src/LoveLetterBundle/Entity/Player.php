<?php

namespace LoveLetterBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use LoveLetterBundle\Entity\Game;
use Doctrine\DBAL\Types\BooleanType;
use Doctrine\DBAL\Types\SmallIntType;


/**
 * Player
 *
 * @ORM\Table(name="player")
 * @ORM\Entity(repositoryClass="LoveLetterBundle\Repository\PlayerRepository")
 */
class Player
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="UserBundle\Entity\User")
     */
    private $user;
    
    /**
     * @var LoveLetterBundle\Entity\Game
     * 
     * @ORM\ManyToOne(targetEntity="LoveLetterBundle\Entity\Game", inversedBy="players")
     */
    private $game;
    
    /**
     * @var BooleanType
     * 
     * @ORM\Column(name="isInRound", type="boolean")
     */
    private $isInRound;
    
    /**
     * @var BooleanType
     * 
     * @ORM\Column(name="isDraw", type="boolean")
     */
    private $isDraw;
    
    /**
     * @var BooleanType
     *
     * @ORM\Column(name="isProtected", type="boolean")
     */
    private $isProtected;
    
    /**
     * @var String
     * 
     * @ORM\Column(name="username", type="string")
     */
    private $username;
    
    /**
     * @var int
     * 
     * @ORM\Column(name="score", type="integer")
     */
    private $score;
    
    /**
     * Constructor
     */
    public function __construct($user)
    {
        $this->isInRound = true;
        $this->isProtected = false;
        $this->username = $user->getUsername();
        $this->score = 0;
        $this->isDraw = false;
        $this->user = $user;
    }
    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isInRound
     *
     * @param boolean $isInRound
     *
     * @return Player
     */
    public function setIsInRound($isInRound)
    {
        $this->isInRound = $isInRound;

        return $this;
    }

    /**
     * Get isInRound
     *
     * @return boolean
     */
    public function getIsInRound()
    {
        return $this->isInRound;
    }

    /**
     * Set isDraw
     *
     * @param boolean $isDraw
     *
     * @return Player
     */
    public function setIsDraw()
    {
        $this->isDraw = !$this->isDraw;

        return $this;
    }

    /**
     * Get isDraw
     *
     * @return boolean
     */
    public function getIsDraw()
    {
        return $this->isDraw;
    }

    /**
     * Set isProtected
     *
     * @param boolean $isProtected
     *
     * @return Player
     */
    public function setIsProtected($isProtected)
    {
        $this->isProtected = $isProtected;

        return $this;
    }

    /**
     * Get isProtected
     *
     * @return boolean
     */
    public function getIsProtected()
    {
        return $this->isProtected;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return Player
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set score
     *
     * @param integer $score
     *
     * @return Player
     */
    public function setScore($score)
    {
        $this->score = $score;

        return $this;
    }

    /**
     * Get score
     *
     * @return integer
     */
    public function getScore()
    {
        return $this->score;
    }
    
    public function addScore() {
        $this->score += 1;
    }

    /**
     * Set user
     *
     * @param \UserBundle\Entity\User $user
     *
     * @return Player
     */
    public function setUser(\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set game
     *
     * @param \LoveLetterBundle\Entity\Game $game
     *
     * @return Player
     */
    public function setGame(\LoveLetterBundle\Entity\Game $game = null)
    {
        $this->game = $game;

        return $this;
    }

    /**
     * Get game
     *
     * @return \LoveLetterBundle\Entity\Game
     */
    public function getGame()
    {
        return $this->game;
    }
}
