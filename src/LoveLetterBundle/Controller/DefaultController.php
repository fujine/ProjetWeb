<?php

namespace LoveLetterBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use LoveLetterBundle\Entity\Game;
use LoveLetterBundle\Entity\Player;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use LoveLetterBundle\Entity\History;

class DefaultController extends Controller
{   
    /**
     * @Route("new/{lobbyId}", requirements={"lobbyId": "\d+"})
     */
    public function newGameAction($lobbyId)
    {
        //Récupération du lobby
        $repository = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('CoreBundle:Lobby')
        ;
        
        $lobby = $repository->getLobbyWithUsers($lobbyId);
        
        if (null === $lobby) {
            throw new NotFoundHttpException("Le lobby ".$lobbyId." n'existe pas.");
        }
        
        if ( $lobby->getHostUsername() != $this->getUser()->getUsername() ) {
            throw new NotFoundHttpException("Vous ne possedez pas ce lobby.");
        }
                
        //Création de la game
        $em = $this->getDoctrine()->getManager();
        $game = new Game();
        $em->persist($game);

        //Ajout des joueurs
        foreach ($lobby->getUsers() as $user) {
            $player = new Player($user);
            $user->setPlayer($player);
            $user->setLobby(null);
            $game->addPlayer($player);
            $em->persist($player);
        }
        
        //Mieux ??
        $em->flush();
        $lobby->setgameId( $game->getId() );
        $game->setup();
        $em->flush();

        return $this->redirectToRoute('loveletter_default_view', array(
            'id' => $game->getId()
        ));
    }
    
    /**
     * @Route("play/{id}", requirements={"id": "\d+"})
     */
    public function playCardAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $repository = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('LoveLetterBundle:Game')
        ;
        
        $game = $repository->getGameWithPlayersCardsHistory($id);
        if ($request->getMethod() == 'POST') {
            $CardId = $request->request->get('cardId');
            $PlayerTargetId = $request->request->get('selectedPlayer');
            $SelectedCard = $request->request->get('selectedCard');
            $playerId = $this->getUser()->getPlayer()->getId();
            $info = $game->play($playerId, $CardId, $PlayerTargetId, $SelectedCard);
            foreach ($info as $line) {
                $request->getSession()->getFlashBag()->add('notice', $line );
            }
            $em->flush();
        }
        
        return $this->redirectToRoute('loveletter_default_view', array(
            'id' => $game->getId()
        ));
    }
    
    /**
     * @Route("/draw/{id}", requirements={"id": "\d+"})
     */
    public function drawAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $repository = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('LoveLetterBundle:Game')
        ;
        $game = $repository->getGameWithPlayersCardsHistory($id);
        
        $game->drawControleur($this->getUser()->getPlayer());
        
        $em->flush();
       
        return $this->redirectToRoute('loveletter_default_view', array(
            'id' => $game->getId()
        ));
    }
    
    /**
     * @Route("/quit")
     *
     */
    public function quitAction(Request $request)
    {        
        $player = $this->getUser()->getPlayer();
        if ($player == null) {
            throw new NotFoundHttpException("Vous ne participez pas à une partie");
        }
        
        $gameId = $player->getGame()->getId();
        
        $repository = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('LoveLetterBundle:Game')
        ;
        $game = $repository->getGameWithPlayersCardsHistory($gameId);
        
        $game->playerLeave($player);
        $player = $this->getUser()->getPlayer();
        $player->setUser(null);
        $this->getUser()->setPlayer(null);
        
        $em = $this->getDoctrine()->getManager();
        $em->flush();
        
        $request->getSession()->getFlashBag()->add(
            'notice', 'Vous avez quitté la partie !' 
            );
        
        return $this->redirectToRoute('core_index_index');
    }
   
    /**
     * @Route("/view/{id}", requirements={"id": "\d+"})
     *
     */
    public function viewAction(Request $request, $id)
    {
        $repository = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('LoveLetterBundle:Game')
        ;
        $game = $repository->getGameWithPlayersCardsHistory($id);
        
        $player = $this->getUser()->getPlayer();
        if( $player != null && ! $game->isPlayerInGame($player) ) {
            throw new NotFoundHttpException("Vous ne participez pas à cette partie.");
        }
        
        if ( !$game->getIsOver() ) {
            return $this->render('LoveLetterBundle:Game:viewGame.html.twig', [
                "game" => $game,
                "idPlayer" => $player->getId(),
            ]);
        } else {
            if ($player !== null) {
                $this->getUser()->setPlayer(null);
                $player->setUser(null);
                $game->playerLeave($player);
                $this->getDoctrine()->getManager()->flush();
            }
            $request->getSession()->getFlashBag()->add(
                'notice', 'La partie est terminée !'
                );
            return $this->redirectToRoute('loveletter_default_viewended', array(
                'id' => $game->getId()
            ));
        }
    }
    
    /**
     * @Route("/viewEnded/{id}", requirements={"id": "\d+"})
     *
     */
    public function viewEndedAction($id)
    {
        $repository = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('LoveLetterBundle:Game')
        ;
        $game = $repository->getGameWithPlayersCardsHistory($id);
        
        return $this->render('LoveLetterBundle:Game:viewEndedGame.html.twig', [
            "game" => $game
            ]);
    }
    
    
    
    /**
     * @Route("/update/{id}", requirements={"id": "\d+"})
     */
    public function updateAction($id)
    {
        $repository = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('LoveLetterBundle:History')
        ;
        $size = $repository->getHistorySize($id);
        
        return new JsonResponse(array('historySize' => $size ));
    } 

}