<?php

namespace CoreBundle\Controller;

use CoreBundle\Entity\Lobby;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class LobbyController extends Controller
{
    /**
     * @Route("/lobby/{lobbyId}", requirements={"lobbyId": "\d+"})
     */
    public function lobbyAction($lobbyId)
    {
        $repository = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('CoreBundle:Lobby')
        ;
        
        $lobby = $repository->getLobbyWithUsers($lobbyId);
        
        if (null === $lobby) {
            throw new NotFoundHttpException("Le lobby ".$lobbyId." n'existe pas.");
        }
        
        return $this->render('CoreBundle:Lobby:lobby.html.twig', [
            'lobby' => $lobby
        ]);
    }
    
    /**
     * @Route("/lobby/create/{nb}", requirements={"nb": "[2-4]"})
     */
    public function createLobbyAction($nb)
    {
        $em = $this->getDoctrine()->getManager();
        
        if ($this->getUser()->getPlayer() != null) {
            throw new NotFoundHttpException("Vous avez une partie en cours");
        }
        
        $lobby = new Lobby();
        $lobby->setPlayerNumber($nb);
        $lobby->addUser($this->getUser());
        $lobby->setHostUsername($this->getUser()->getUsername());
        $em->persist($lobby);
        $em->flush();
        
        return $this->redirectToRoute('core_lobby_lobby', array(
            'lobbyId' => $lobby->getId()
        ));
    }
    
    /**
     * @Route("/lobby/delete/{lobbyId}", requirements={"lobbyId": "\d+"})
     */
    public function deleteLobbyAction($lobbyId)
    {
        $repository = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('CoreBundle:Lobby')
        ;
        
        $lobby = $repository->getLobbyWithUsers($lobbyId);
        
        if (null === $lobby) {
            throw new NotFoundHttpException("Le lobby ".$lobbyId." n'existe pas.");
        }
        
        if ( $lobby->getHostUsername() != $this->getUser()->getUsername() ) {
            throw new NotFoundHttpException("Vous ne possedez pas ce lobby.");
        } else {
            $em = $this->getDoctrine()->getManager();
            foreach ($lobby->getUsers() as $user) {
                $user->setLobby(null);
            }
            $em->remove($lobby);
            $em->flush();
        }
        
        return $this->redirectToRoute('core_index_index');
        
    }
    
    /**
     * @Route("/lobby/addUser/{lobbyId}", requirements={"lobbyId": "\d+"})
     */
    public function addUserAction($lobbyId)
    {
        $repository = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('CoreBundle:Lobby')
        ;
        
        $lobby = $repository->getLobbyWithUsers($lobbyId);
        
        if (null === $lobby) {
            throw new NotFoundHttpException("Le lobby ".$lobbyId." n'existe pas.");
        }
        
        if ($this->getUser()->getPlayer() != null) {
            throw new NotFoundHttpException("Vous avez une partie en cours");
        }
        
        /* Test si le joueur est déjà dans le lobby */
        if( $lobby->isUserInLobby($this->getUser()) === false ) {
            $em = $this->getDoctrine()->getManager();
            $lobby->addUser($this->getUser());
            $em->flush();
        }
        
        return $this->redirectToRoute('core_lobby_lobby', array(
            'lobbyId' => $lobby->getId()
        ));
        
    }
    
    /**
     * 
     * @Route("/lobby/addPlayerNumber/{lobbyId}", requirements={"lobbyId": "\d+"})
     */
    public function addPlayerAction($lobbyId)
    {
        $repository = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('CoreBundle:Lobby')
        ;
        
        $lobby = $repository->getLobbyWithUsers($lobbyId);
        
        if (null === $lobby) {
            throw new NotFoundHttpException("Le lobby ".$lobbyId." n'existe pas.");
        }
        
        if ( $lobby->getHostUsername() != $this->getUser()->getUsername() ) {
            throw new NotFoundHttpException("Vous ne possedez pas ce lobby.");
        } else {
            $em = $this->getDoctrine()->getManager();
            $lobby->addPlayerNumber();
            $em->flush();
        }
        
        return $this->redirectToRoute('core_lobby_lobby', array(
            'lobbyId' => $lobby->getId()
        ));
    }
    
    /**
     *
     * @Route("/lobby/removePlayerNumber/{lobbyId}", requirements={"lobbyId": "\d+"})
     */
    public function removePlayerAction($lobbyId)
    {
        $repository = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('CoreBundle:Lobby')
        ;
        
        $lobby = $repository->getLobbyWithUsers($lobbyId);
        
        if (null === $lobby) {
            throw new NotFoundHttpException("Le lobby ".$lobbyId." n'existe pas.");
        }
        
        if ( $lobby->getHostUsername() != $this->getUser()->getUsername() ) {
            throw new NotFoundHttpException("Vous ne possedez pas ce lobby.");
        } else {
            $em = $this->getDoctrine()->getManager();
            $lobby->removePlayerNumber();
            $em->flush();
        }
        
        return $this->redirectToRoute('core_lobby_lobby', array(
            'lobbyId' => $lobby->getId()
        ));
    }
    
    /**
     * @Route("/lobby/update/{lobbyId}", requirements={"lobbyId": "\d+"})
     */
    public function updateAction($lobbyId)
    {
        $repository = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('CoreBundle:Lobby')
        ;
        
        $lobby = $repository->find($lobbyId);
        
        if (null === $lobby) {
            throw new NotFoundHttpException("Le lobby ".$lobbyId." n'existe pas.");
        }
        
        return new JsonResponse(array(
            'historySize' => $lobby->getHistorySize(),
            'gameId' => $lobby->getGameId()
        ));
    }
}
