<?php

namespace CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use UserBundle\Entity\User;

/**
 * Lobby
 *
 * @ORM\Table(name="lobby")
 * @ORM\Entity(repositoryClass="CoreBundle\Repository\LobbyRepository")
 */
class Lobby
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="PlayerNumber", type="smallint")
     */
    private $playerNumber;
    
    /**
     * @var string
     *
     * @ORM\Column(name="hostName", type="string")
     */
    private $hostUsername;
    
    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="UserBundle\Entity\User", mappedBy="lobby", cascade={"persist", "refresh"})
     */
    private $users;
    
    /**
     * @var int
     * 
     * @ORM\Column(name="HistorySize", type="smallint")
     */
    private $historySize;
    
    /**
     * @var int
     * 
     * @ORM\Column(name="GameBegin", type="smallint", nullable=true)
     */
    private $gameId;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set playerNumber
     *
     * @param integer $playerNumber
     *
     * @return Lobby
     */
    public function setPlayerNumber($playerNumber)
    {
        $this->playerNumber = $playerNumber;
        $this->historySize++;
        return $this;
    }

    /**
     * Get playerNumber
     *
     * @return int
     */
    public function getPlayerNumber()
    {
        return $this->playerNumber;
    }
    
    /**
     * Set hostUsername
     *
     * @param string $hostUsername
     *
     * @return Lobby
     */
    public function setHostUsername($hostUsername)
    {
        $this->hostUsername = $hostUsername;
        
        return $this;
    }
    
    /**
     * Get hostUsername
     *
     * @return string
     */
    public function getHostUsername()
    {
        return $this->hostUsername;
    }

    /**
     * Add user
     *
     * @param \UserBundle\Entity\User $user
     *
     * @return Lobby
     */
    public function addUser(\UserBundle\Entity\User $user)
    {
        $this->users[] = $user;
        $user->setLobby($this);
        $this->historySize++;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \UserBundle\Entity\User $user
     */
    public function removeUser(\UserBundle\Entity\User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }
    
    /**
     * Set historySize
     *
     * @param integer $historySize
     *
     * @return Lobby
     */
    public function setHistorySize($historySize)
    {
        $this->historySize = $historySize;
        
        return $this;
    }
    
    /**
     * Get historySize
     *
     * @return integer
     */
    public function getHistorySize()
    {
        return $this->historySize;
    }
    
    /**
     * Set gameId
     *
     * @param integer $gameId
     *
     * @return Lobby
     */
    public function setGameId($gameId)
    {
        $this->gameId = $gameId;
        $this->historySize++;
        
        return $this;
    }
    
    /**
     * Get gameId
     *
     * @return integer
     */
    public function getGameId()
    {
        return $this->gameId;
    }
    
    public function  __construct()
    {
        $this->playerNumber = 2;
        $this->historySize = 0;
        $this->gameBegin = false;
    }
    
    public function getWaitingUsers()
    {
        return $this->playerNumber - $this->users->count();
    }
    
    public function addPlayerNumber()
    {
        if($this->playerNumber < 4) {
            $this->playerNumber++;
            $this->historySize++;
        }
    }
    
    public function removePlayerNumber()
    {
        if($this->playerNumber > 2) {
            $this->playerNumber--;
            $this->historySize++;
        }
    }
    
    /**
     * 
     * @param \UserBundle\Entity\User $user
     * @return boolean true si l'utilisateur est déjà dans le lobby
     */
    public function isUserInLobby(\UserBundle\Entity\User $user)
    {
        return $this->users->contains($user);
    }
}
